using Nethereum.Web3.Accounts.Managed;
using Nethereum.Web3;
using ContractInterface.Common.Entities;

namespace GamerSmartToken.WebApi.Services
{
    public interface IAccountService
    {
        AccountDAO Authenticate(string address, string password);
        ManagedAccount GetAccount();
        Web3 GetWeb3();
    }
}