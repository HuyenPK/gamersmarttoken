namespace GamerSmartToken.WebApi.Controllers.Request.Wrappers
{
    public class ContractRequest
    {
        public string ContractAddress{get; set;}
        public string ContractName{get; set;}
        public bool IsDeployed{get; set;}
    }
}