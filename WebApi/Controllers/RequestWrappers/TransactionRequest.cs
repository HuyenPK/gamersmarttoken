using System.Numerics;

namespace GamerSmartToken.WebApi.Controllers.Request.Wrappers
{
    public class TransactionRequest
    {
        public string FromAddress{get; set;}
        public string ToAddress{get; set;}
        public BigInteger Amount{get; set;}
    }
}