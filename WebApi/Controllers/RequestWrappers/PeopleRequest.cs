using System.Numerics;

namespace GamerSmartToken.WebApi.Controllers.Request.Wrappers
{
    public class PeopleRequest
    {
        public string ToAdd{get; set;}
        public string ToRemove{get; set;}
        public string ToInquire{get; set;}
        public string MapTo{get; set;}
        public BigInteger Allowance{get; set;}
    }
}